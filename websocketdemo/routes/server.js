const http = require('http');
const express = require('express');
const socket = require('socket.io');
const app = express();
const port = 8088;
var router = express.Router();


let server = http.createServer(app);

let io = socket(server);

io.on('connection', function (socket) {
    console.log('a user connect');

    socket.on('disconnect', function () {
        console.log("a user go out ");
    });

    socket.on("msg", function (obj) {
        setTimeout(function () {
            console.log("the websocket message is" + obj)
        },3000)
    });
});

server.listen(port);

router.get('/imroom',function (req, res, next) {
    // let i = 0;
    // setInterval(() => {
    //     i++;
    //     res.send(`socket:${i}`);
    // },1000);
    res.send(`socket:`);
});

module.exports = router;