const http = require('http');
const express = require('express');
const app = express();
var router = express.Router();

let server = http.createServer(app);

var WebSocketServer = require('ws').Server;

wss = new WebSocketServer({ port: 8181});

wss.on('connection',function (ws) {
    console.log('websocket连接已建立。。。');

    let i = 0;
    let mess;

    ws.on('message',function (message) {
        console.log('websocket信息传输中。。。',message);
        mess = message;
        // ws.send(mess + '' + i)
        setInterval(()=> {
            i++;
            ws.send(mess + '' + i)
        },3000);
    });


    ws.on('close',() => {
        console.log('websocket连接已关闭。。。');
    });

    ws.on("error", error => {
        console.log(error);
    })
});


router.get('/ws',function (req, res, next) {
    // let i = 0;
    // setInterval(() => {
    //     i++;
    //     res.send(`socket:${i}`);
    // },1000);


    res.send('haha');
});

module.exports = router;